var app = new Vue({
  el: "#app",

  data: {
    usuario: '',

  },

  created: function(){
  
  },

  mounted: function(){
    
  },
 
  methods: {
    
    testes: function () {
      var self = this;
      //Busca usuario atual
      self.usuario = Usuario.getUsuarioAtual();
      var usuarioAutal = self.usuario;
      var urlGroupByUser = "/api/public/2.0/groups/findGroupsByUser/767w8cl6pisjllia1416230881256";
      //Busca todos os grupos que o usuário participa
      Global.promiseApiFluig(urlGroupByUser).then(function (result) {
        var grupos = JSON.parse(result).content;
        var listPromiseSolicitacao = [];
        for (var i in grupos) {
          if (grupos[i].groupType == "user") {
            var grupoAtual = new Grupo(grupos[i].code, grupos[i].description)
            var urlSolicitacaoByGroup = "/api/public/2.0/tasks/findWorkflowTasks/Pool:Group:" + grupoAtual.idGrupo;
            listPromiseSolicitacao.push(Global.promiseApiFluig(urlSolicitacaoByGroup));
          }
        }
        if (listPromiseSolicitacao.length > 0){
          return Promise.all(listPromiseSolicitacao)
        }
      }).then(function (resultSolicitacao){//Busca as solicitações de cada grupo
        resultSolicitacao = JSON.parse(resultSolicitacao).content;
        var listPromiseFormulario = [];
        for(var i in resultSolicitacao){
          var dataSolicitacao = resultSolicitacao[i]
          var urlFormSolicitacao = "/api/public/2.0/documents/getActive/" + dataSolicitacao.mainAttachmentDocumentId;
          listPromiseFormulario.push(Global.promiseApiFluig(urlFormSolicitacao));
        }
        if (listPromiseFormulario.length > 0){
          return Promise.all(listPromiseFormulario)
        }
      }).then(function(resultForm){//Busca o dataset de cada solicitação
        var listPromiseDataset = [];
        for(var i in resultForm){
          var row = JSON.parse(resultForm[i])
          var formDocumentId =  row.content.parentDocumentId;
          var formVersion = row.content.version;
          var urlGetDataset = "/ecm/api/rest/ecm/cardIndexPublisher/getItem/" + formDocumentId + "/" + formVersion;
          listPromiseDataset.push(Global.promiseApiFluig(urlGetDataset))
        }
        if(listPromiseDataset.length > 0){
          return Promise.all(listPromiseDataset)
        }
      }).then(function(resultDataset){//
        for (var i in resultDataset) {
          var datasetName = JSON.parse(resultDataset[i]).infGeralVO.datasetName;

        }
      });
      //Busca os grupos do usuario atual
      /*
      var c1Doc = DatasetFactory.createConstraint("documentPK.documentId",81129,81129,ConstraintType.MUST);
var c2Doc = DatasetFactory.createConstraint("activeVersion",true,true,ConstraintType.MUST);

// Busca enquetes na pasta de preferencia
var dtsListSurvey = DatasetFactory.getDataset("document", ["datasetName"], new Array(c1Doc, c2Doc), null);
      
      Grupo.getGruposByUser(usuarioAutal.idUsuario, function (result) {
        if (result.status == 200) {
          var grupos = result.body.content;
          for (var i in grupos) {
            if(grupos[i].groupType == "user"){
             var grupoAtual = new Grupo(grupos[i].code, grupos[i].description)

              //Busca as solicitacoes dos grupos 
              Solicitacao.getSolicitacaoByGrupo(grupos[i].code, function (resultSolicitacao) {
                if (resultSolicitacao.body.content.length > 0) {
                  solicitacoes = resultSolicitacao.body.content;
                  for (var j in solicitacoes) {
                    grupoAtual.listSolicitacao.push(new Solicitacao(solicitacoes[j].processInstanceId, solicitacoes[j].mainAttachmentDocumentId))
                  }
                  usuarioAutal.listGrupo.push(grupoAtual);
                  var c1Doc = DatasetFactory.createConstraint("documentPK.documentId", 81129, 81129, ConstraintType.MUST);
                  var c2Doc = DatasetFactory.createConstraint("activeVersion", true, true, ConstraintType.MUST);

                  // Busca enquetes na pasta de preferencia
                  var dtsListSurvey = DatasetFactory.getDataset("document", ["datasetName"], new Array(c1Doc, c2Doc), null);
                }
              })
            }
          }
        } else {
          console.log("error para buscar os grupos");
        }
      });*/
    }
  }

})