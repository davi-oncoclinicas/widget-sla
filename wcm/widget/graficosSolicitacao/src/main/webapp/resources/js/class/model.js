/**
 * Classe Usuario
*/
var Usuario = function (idUsuario, nomeUsuario, emailUsuario) {
    this.idUsuario = idUsuario;
    this.nomeUsuario = nomeUsuario;
    this.emailUsuario = emailUsuario;
    this.listGrupo = [];
};

Usuario.getUsuarioAtual = function(){
    return new Usuario(parent.WCMAPI.userCode, parent.WCMAPI.user, parent.WCMAPI.userEmail);
};

Usuario.prototype.getDataGrafic = function(){
    Vue.$promise.all([
        this.$http.get('url1'),
        this.$http.get('url2')
    ]).then(function (responseArr) {

    });


}

var Solicitacao = function (idSolicitacao, attachmentDocumentId) {
    this.idSolicitacao = idSolicitacao;
    this.attachmentDocumentId = attachmentDocumentId;
}

Solicitacao.getSolicitacaoByGrupo = function(idGrupo, callback){
    var url = "http://" + location.hostname + "/api/public/2.0/tasks/findWorkflowTasks/Pool:Group:" + idGrupo;
    Vue.http.get(url).then(
        //Sucesso
        function (result) {
            callback(result)
        },
        //Error
        function (result) {
            callback(result);
        }
    );
}


var Graphic = function(){

}

Graphic.prototype.promiseGroup = function(){

    return new Promise(function (resolve, reject){

    })

}



Graphic.prototype.prom



var Grupo = function(idGrupo, nomeGrupo){
    this.idGrupo = idGrupo;
    this.nomeGrupo = nomeGrupo;
    this.listSolicitacao = [];
};

Grupo.getGruposByUser = function (idUsuario, callback) {
    var url = "http://" + location.hostname + "/api/public/2.0/groups/findGroupsByUser/767w8cl6pisjllia1416230881256"; //+ idUsuario;
    Vue.http.get(url).then(
        //Sucesso
        function(result){
            callback(result)
        },
        //Error
        function(result){
            callback(result);
        }
    );
}

var Global = function(){

}
/*
 * Cria url de consulta de  dataset
 * */
Global.getUrlDataset = function (datasetName, fields, filter) {
    //Monta url com os parametros
    var that = this;
    var params = new Array();
    var url = '/api/public/ecm/dataset/search?';

    params.push('datasetId=' + datasetName);

    if (fields != null) {
        params.push('resultFields=' + fields.join(','));
    }

    if (filter != null) {
        var search = filter.split('|');
        for (var i in search){
            if(i % 2 == 0){
                params.push('searchField=' + search[i]);
            }else{
                params.push('searchValue=' + search[i]);
            }
        }
    }

    url += params.join('&');
  
    return url
};
/**
 * Faz um requsição get na api do fluig 
 * @param {*} url 
 * @param {*} type 
 * @param {*} callback 
 */
Global.promiseApiFluig = function (url) {
    urlCompleta = "http://" + location.hostname +""+url;
    return new Promise(function (resolve, reject) {
        var req = new XMLHttpRequest();
        req.open('GET', urlCompleta);

        req.onload = function () {
            if (req.status == 200) {
                resolve(req.response);
            } else {
                reject(Error(req.statusText));
            }
        };

        req.onerror = function () {
            reject(Error("Network Error"));
        };
        req.send();
    });
}

Global.POSTPromiseFluig = function(url,dataJson){
    var req = XMLHttpRequest();
    
    req.open("POST", "/json-handler")
    req.setRequestHeader("Content-Type", "application/json")

    req.onload = function () {
        if (req.status == 200) {
            resolve(req.response);
        } else {
            reject(Error(req.statusText));
        }
    };

    req.onerror = function () {
        reject(Error("Network Error"));
    };
    req.send(JSON.stringify(dataJson));
}