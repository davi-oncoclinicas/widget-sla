<div id="MyWidget_${instanceId}" class="super-widget wcm-widget-class fluig-md" data-params="MyWidget.instance()">
  <head>
    <!-- Import Google Icon Font -->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons"
    rel="stylesheet" />
    <link rel="stylesheet"
    href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700"
    type="text/css" />
    <!--Import materialize.css -->
    <link type="text/css" rel="stylesheet"
    href="/widget_sla/resources/css/fluig-materialize.css"
    media="screen,projection" />

    <!--Import animate.css -->
    <link type="text/css" rel="stylesheet"
    href="/widget_sla/resources/css/animate.css" media="screen,projection" />
    <!--Import style.css -->
    <link type="text/css" rel="stylesheet"
    href="/widget_sla/resources/css/style.css" media="screen,projection" />
  </head>
  <body>
    <!--Aplicação -->
      <div id="app" class="fluig-md">
        <div class="row">             
          <div class="col m12">
            <div class="card">
              <div class="card-content">
                <h3 class="card-title">Selecione o chamado</h3>
                <div class="row">

                  <div class="input-field col m4">
                    <i class="Small material-icons">view_comfy</i>
                    <material-select class='selectMaterial' id="selectArea" v-model="selectedArea">
                      <option value="" disabled selected>Selecione uma área</option>
                      <option v-for="option in dataSelect.optionArea" :value="option.value" v-text="option.text"></option>
                    </material-select>
                   <label>Área</label>
                 </div>

                 <div class="input-field col m4">
                   <i class="Small material-icons">view_array</i>
                   <material-select class='selectMaterial' id="selectCategoria"
                   v-bind:disabled="selectedArea == ''" v-model="selectedCategoria">
                     <option value="" disabled selected>Selecione uma Categoria</option>
                     <option v-for="option in dataSelect.optionCategoria" :value="option.value" v-text="option.text"></option>
                   </material-select>
                  <label>Categoria</label>
                </div>

                <div class="input-field col m4">
                  <i class="Small material-icons">view_carousel</i>
                  <material-select class='selectMaterial' id="selectSolicitacao"
                  v-bind:disabled="selectedCategoria == ''" v-model="selectedSolicitacao">
                    <option value="" disabled selected>Selecione uma Solicitação</option>
                    <option v-for="option in dataSelect.optionSolicitacao" :value="option.value" v-text="option.text"></option>
                  </material-select>
                 <label>Solicitação</label>
                </div>

                </div>
              </div>
            </div>
          </div>
          <div class="col m12" v-if="solicitacao == ''">
            <p class="center-align">Nehnhuma solicitação encontrada.</p>
          </div>
          <!--Tabela de atividades -->
          <div class="row" v-show="solicitacao != ''">
            <preloader id="loadPage" ref="foo" v-show="isLoad"></preloader>
            <div class="col m12" v-show="!isLoad">
              <div class="card">
                <div class="card-content">
                  <div class="row">
                    <div class="input-field col m4">
                     <input id="slaGlobal" type="text" v-mask="'###:##'" v-model="slaGlobal"/>
                     <label for="slaGlobal">Tempo SLA</label>
                   </div>
                   <div class="col m4">
                    <label>Total SLA</label>
                    <h6 class="center-align">{{totalSLA}}</h6>
                  </div>
                   <div class="input-field col m4">
                     <a class="waves-effect waves-light btn right" v-on:click="saveSla"><i class="material-icons left">save</i>Salvar</a>
                   </div>
                  </div>
                  <div class="row">
                    <table class="striped">
                      <thead>
                        <tr>
                          <th class="col m2 center-align"> </th>
                          <th class="col m8 center-align">Atividade</th>
                          <th class="col m2 center-align">SLA</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr v-for="(task, index) in solicitacao.listTasks">
                          <td class="col m2 center-align" style="margin-top:15px;">
                            <p>
                              <label>
                                <input type="checkbox" class="filled-in" :id="'check'+task.id" v-model="formData.fieldsCheckSla[index]" v-bind:value="task.id" />
                                <span></span>
                              </label>
                            </p>
                          </td>
                          <td class="col m8 center-align" style="margin-top:15px;">
                            <p class="descTask" >{{task.name}}</p>
                          </td>
                          <td class="col m2 center-align" >
                             <input class="textCenter" v-mask="'###:##'" placeholder="hhh:mm" v-bind:id="task.idTask" v-model="task.sla" />
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--Scripts -->
      <!--<script type="text/javascript"src="/widget_sla/resources/js/lib/jquery/jquery-3.3.1.min.js?100"></script>-->
      <script type="text/javascript" src="/webdesk/vcXMLRPC.js?100"></script>
      <script type="text/javascript" src="/widget_sla/resources/js/lib/materialize/materialize.js?100"></script>
      <script type="text/javascript" src="/widget_sla/resources/js/lib/vue/vue.js?100"></script>
      <script type="text/javascript" src="/widget_sla/resources/js/lib/vue/vue-resource.min.js?100"></script>
      <script type="text/javascript" src="/widget_sla/resources/js/lib/vue/VueTheMask.umd.js?100"></script>
      <script type="text/javascript" src="/widget_sla/resources/js/class/model.js?100"></script>
      <script type="text/javascript" src="/widget_sla/resources/js/component.js?100"></script>
      <script type="text/javascript" src="/widget_sla/resources/js/app.js?100"></script>
  </body>
</div>
