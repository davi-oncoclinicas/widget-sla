//Aplicação principal
var app = new Vue({
  el:"#app",

  data : {
    controller: new Global(),
    solicitacao: '',
    selectedArea : '',
    selectedCategoria : '',
    selectedSolicitacao : '',
    slaGlobal : '',
    dataSelect : {
      optionArea : [],
      optionCategoria : [],
      optionSolicitacao : []
    },
    formData:{
      fieldsCheckSla:{}
    },
    isLoad: false

  },
  created:function(){
    this.isLoad = true;
    this.loadDataArea();
    this.isLoad = false;
  },
  mounted: function(){
    //remove titulo da widget
    $(".titleArea").parent().hide();
  },
  computed:{
    totalSLA:function(){
      var listSla = this.solicitacao.listTasks;
      if (listSla == undefined || listSla.length == 0) {
        return  "00:00"
      }else{
        var totalHoras = 0;
        var totalMinutos = 0;
        for(var i in listSla){
          if (listSla[i].sla != ""){
            var tempoSla = listSla[i].sla.split(':');
            totalHoras += parseInt(tempoSla[0]);
            totalMinutos += parseInt(tempoSla[1]);
          }
        }
        var minutos = totalMinutos % 60;
        totalHoras += (totalMinutos - minutos) / 60;
        if((minutos+"").length == 1){
          minutos = "0"+minutos;
        }
        return isNaN(totalMinutos) ? 'Processando...' : totalHoras+":"+minutos;
      }
    }
  },
  methods:{

    // Load data from database
		loadDataArea: function() {
			var self = this;
			var url = self.controller.getServiceUrl() + '?transform=1&filter=idp,eq,0&order=item';

			Vue.http.get(url).then(
				//success
				function (response) {
					var data = response.body;
          self.dataSelect.optionArea = [];

					for(var i = 0; i < data.servicos.length; i++) {
						var row = data.servicos[i];
            self.dataSelect.optionArea.push({'value': row.id,'text': row.item});
					}
				},
				//error
				function(response) {
					throw 'JSON Error: Url: ' + url + ' Body: ' + JSON.stringify(response);
				});
			},

      loadDataCategoria: function() {
        var self = this;
        var url = self.controller.getServiceUrl() + '?transform=1&filter=idp,eq,' + self.selectedArea + '&order=item';
        self.dataSelect.optionCategoria = [];
        self.dataSelect.optionSolicitacao = [];
        if(self.selectedArea != '') {
          Vue.http.get(url).then(
            //success
            function (response) {
              var data = response.body;
              for(var i = 0; i < data.servicos.length; i++) {
                var row = data.servicos[i];
                self.dataSelect.optionCategoria.push({'value': row.id,'text': row.item});
              }
            },
            //error
            function(response) {
              throw 'JSON Error: Url: ' + url + ' Body: ' + JSON.stringify(response);
            });
        } else {
          self.dataSelect.optionCategoria = [];
        }
      },
      loadDataSolicitacao: function() {
        var self = this;
        var url = self.controller.getServiceUrl() + '?transform=1&filter=idp,eq,' + self.selectedCategoria + '&order=item';
        if(self.selectedCategoria != '') {
          Vue.http.get(url).then(
            // success
            function(response) {
              var data = response.body;
              for(var i = 0; i < data.servicos.length; i++) {
                var row = data.servicos[i];
                if(row.url.toLowerCase().indexOf("id=") != -1){
                  var idSolicitacao = row.url;
                  idSolicitacao = idSolicitacao.substring(idSolicitacao.toLowerCase().indexOf("id=") + 3);
                  if (idSolicitacao.indexOf('&') != -1){
                    idSolicitacao = idSolicitacao.substring(0, idSolicitacao.indexOf('&'));
                  }
                  self.dataSelect.optionSolicitacao.push({'value': idSolicitacao+"|"+row.id,'text': row.item});
                }
              }
            },
            //error
            function(response) {
              throw 'JSON Error: Url: ' + url + ' Body: ' + JSON.stringify(response);
            });
        } else {
          self.dataSelect.optionSolicitacao = [];
        }
      },

      saveSla: function(){
        var self = this;
        self.isLoad = !self.isLoad;
        self.solicitacao.saveSla(self.totalSLA, function (response) {
          if(response == 'success'){
            FLUIGC.toast({
              message: "As informações foram salvas com sucesso!",
              timeout: 3000,
              type: 'success'
            });
            self.clearFields();
          }else{
            FLUIGC.toast({
              message: "Algo deu errado e não foi possível salvar as informações!",
              timeout: 3000,
              type: 'danger'
            });
          }
          self.isLoad = !self.isLoad;
        });
      },
      clearFields : function(){
        this.solicitacao = '';
        this.selectedSolicitacao = '';
      },
      removeAllSpace : function(str){
        return str.replace( /\s/g, '' );
      }
  },
  watch: {
    selectedArea : function(newData){
      if(newData != ''){
        this.loadDataCategoria();
      }
    },
    selectedCategoria:function(newData){
      if(newData != ''){
        this.loadDataSolicitacao();
      }
    },
    selectedSolicitacao:function(newData){
      if(newData != ''){
        var idSolicitacao = newData.split('|')[0];
        var codigo = newData.split('|')[1];
        this.solicitacao = new SolicitacaoDAO(idSolicitacao, codigo);
        this.solicitacao.getTasks();
      }
    },
    slaGlobal:function(newData){
      for(var i in this.formData.fieldsCheckSla){
        if(this.formData.fieldsCheckSla[i] == true){
          this.solicitacao.listTasks[i].sla = newData;
        }
      }
    }
  }

})
