function getSla() {
    try {
        var intensUrl = parent.window.location.search;
        var tipoFiltro = '';
        var filtro = '';
        if (intensUrl.indexOf('ics=') != -1) {
            tipoFiltro = 'codigo';
            filtro = intensUrl.substring(intensUrl.indexOf('ics=') + 4);
        } else {
            tipoFiltro = 'idSolicitacao';
            filtro = intensUrl.substring(intensUrl.toLowerCase().indexOf('id=') + 3);
        }
        var ics = intensUrl.substring(intensUrl.indexOf('ics=') + 4);
        var c1 = DatasetFactory.createConstraint(tipoFiltro, filtro, filtro, ConstraintType.MUST);
        var dsSla = DatasetFactory.getDataset('ds_SLA', null, new Array(c1), null).values;
        var list_sla;
        var total_sla;
        if (dsSla.length > 0) {
            list_sla = JSON.parse(dsSla[0].listAtividade);
            total_sla = dsSla[0].totalSLA;
            for (var i in list_sla) {
                var row = list_sla[i];
                $("[name='SLA" + row.id + "']").val(row.sla);
            }
            $("[name='TOTAL_SLA']").val(total_sla);
        }
    } catch (e) {
        console.log("Erro para buscar o SLA das atividades.")
    }
}