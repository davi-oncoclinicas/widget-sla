/**
*Classe solicacao
**/
var ID_FORMULARIO = '1373129'; //Id do formulário formSLA - DEV: 1528832 TST: 1373129

var SolicitacaoDAO = function(idSolicitacao, codigo){
  this.idSolicitacao = idSolicitacao;
  this.codigo = codigo;
  this.listTasks = [];
  this.discardTask = ['Aprovado','Fim','Inicio','Exclusivo','Intermediário','Início']
}

SolicitacaoDAO.prototype.getTasks = function(callback) {
  var self = this;
  var tasks = [];
  var slaSolcitacao = self.getSlaExistente();
  
  //busca as atividades da ultima versão do fluxo
  self.getCurrentVersion(function (version) {
    version = version + '';
    var c1 = DatasetFactory.createConstraint('processStatePK.processId', self.idSolicitacao, self.idSolicitacao, ConstraintType.MUST);
    var c2 = DatasetFactory.createConstraint('processStatePK.version', version, version, ConstraintType.MUST);
    var ds = DatasetFactory.getDataset('processState', new Array('stateName', 'processStatePK.sequence'), new Array(c1, c2), null);
    
    var listTaskExistente = self.getSlaExistente().listAtividade;
    if (listTaskExistente != undefined){
      listTaskExistente = JSON.parse(listTaskExistente);
    }else{
      listTaskExistente = [];
    }

    for (var i in ds.values) {
      var task = ds.values[i].stateName;
      var idTask = ds.values[i]['processStatePK.sequence'];
      //remove exclusivos e atividades de servico
      if (task.indexOf('?') == -1) {
        for (var j in self.discardTask) {
          if (task == self.discardTask[j]) {
            task = '';
            break;
          }
        }
        //busca sla existente
        if (task != '') {
          var sla = '';
          for(var i in listTaskExistente){
            if (listTaskExistente[i].id == idTask){
              sla = listTaskExistente[i].sla;
              break;
            }
          }
          tasks.push(new Task(idTask,task,sla));
        }
      }
    }
    self.listTasks = tasks;
    if (callback != null) {
      callback(tasks);
    }
  });

}

SolicitacaoDAO.prototype.getCurrentVersion = function(callback){
  var self = this;
  var url = 'http://'+location.hostname+'/api/public/2.0/workflows/getProcessVersion/'+this.idSolicitacao;

  Vue.http.get(url).then(
    //success
    function (response) {
      callback(response.body.content);
    },
    //error
    function(response) {
      throw 'JSON Error: Url: ' + url + ' Body: ' + JSON.stringify(response);
    }
  );
}

SolicitacaoDAO.prototype.saveSla = function(totalSLA, callback){
  var self = this;
  var dsSla = self.getSlaExistente();
  var url;
  var dataSla;
  if (dsSla == false) {//Cria instancia para armazenar sla
    url = "/api/public/2.0/cards/create";
    dataSla = JSON.stringify({
        "documentDescription": self.idSolicitacao,
        "parentDocumentId": ID_FORMULARIO,
        "version": 1000,
        "inheritSecurity": true,
        "formData": [{
          "name": "idSolicitacao",
          "value": self.idSolicitacao
        }, {
          "name": "codigo",
          "value": self.codigo
        }, {
          "name": "listAtividade",
          "value": JSON.stringify(self.listTasks)
        }, {
          "name": "totalSLA",
          "value": totalSLA
        }]
    })
      
  }else{//Atualiza o sla da solicticao
    url = "/ecm/api/rest/ecm/cardView/editCard/" + dsSla.documentid + "/" + dsSla.version;
    dataSla = JSON.stringify([
      {
        "name": "idSolicitacao",
        "value": self.idSolicitacao
      }, {
        "name": "codigo",
        "value": self.codigo
      }, {
        "name": "listAtividade",
        "value": JSON.stringify(self.listTasks)
      }, {
        "name": "totalSLA",
        "value": totalSLA
      }
    ])

  }

  $.ajax({
    async: false,
    type: "POST",
    url: url,
    contentType: "application/json",
    data: dataSla
  }).done(function (data) {
      callback('success');
  }).fail(function(data){
      callback('error');
      console.log(data);
  });
}

SolicitacaoDAO.prototype.getSlaExistente = function () {

  var c1 = DatasetFactory.createConstraint('codigo', this.codigo, this.codigo, ConstraintType.MUST);
  var c2 = DatasetFactory.createConstraint('idSolicitacao', this.idSolicitacao, this.idSolicitacao, ConstraintType.MUST);
  var c3 = DatasetFactory.createConstraint('metadata#card_index_version', 'true', 'true', ConstraintType.MUST);
  var ds_SLA = DatasetFactory.getDataset('ds_SLA', null, new Array(c1,c2,c3), null);

  if(ds_SLA.values.length > 0){
    return ds_SLA.values[0];
  }else{
    return false
  }
}

/**
 *Classe de atividades de uma solicitacao
 **/
var Task = function(id, name, sla){
  this.id = id;
  this.name = name;
  this.sla = sla;
}

/**
*Classe Global
**/
var Global = function(){
  this.serviceUrlHom = 'http://catalogoservicoshom.oncoclinicas.com.br/public/api.php/servicos';
  this.serviceUrlPrd = 'http://catalogoservicos.oncoclinicas.com.br/public/api.php/servicos';
  
}

Global.prototype.getServiceUrl = function() {
  return (location.hostname === 'oncoclinicas.fluig.com') ? this.serviceUrlPrd : this.serviceUrlHom;
}
