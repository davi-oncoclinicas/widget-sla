
//Select para funcionar com o materialcss
Vue.component("material-select", {
    template: '<select><slot></slot></select>',
    props: ['value'],
    watch: {
        value: function (value) {

            this.relaod(value);
        }
    },
    methods:{
      relaod : function (value) {

          var select = $(this.$el);

          select.val(value || this.value);
          select.material_select('destroy');
          select.material_select();
      },
      
    },
    mounted: function () {

        var vm = this;
        var select = $(this.$el);

        select
            .val(this.value)
            .on('change', function () {

                vm.$emit('input', this.value);
            });

        select.material_select();
    },
    updated: function () {

        this.relaod();
    },
    destroyed: function () {

        $(this.$el).material_select('destroy');
    }
});

Vue.component('preloader', {
    template: ' <div class="preloader-background preloader-card center-align">' +
        '           <div class="preloader-wrapper big active preloader-card">' +
        '               <div class="spinner-layer spinner-blue-only">' +
        '                   <div class="circle-clipper left">' +
        '                       <div class="circle"></div>' +
        '                   </div>' +
        '                   <div class="gap-patch">' +
        '                       <div class="circle"></div>' +
        '                   </div>' +
        '                   <div class="circle-clipper right">' +
        '                       <div class="circle"></div>' +
        '                   </div>' +
        '               </div>' +
        '           </div>' +
        '       </div>',
    methods: {
        show: function (id) {
            $('#' + id).fadeIn('slow');
            $('#' + id).fadeIn();
        },
        hide: function (id) {
            $('#' + id).fadeOut('slow');
            $('#' + id).fadeOut();
        }
    }
});
